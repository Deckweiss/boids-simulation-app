package com.jimdo.welcomeghosts.boidssimulation;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

/**
 * Created by Deckweiss on 07.09.2015.
 */
public class WallpaperPrefsActivity extends PreferenceActivity {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    @Override
    protected boolean isValidFragment(String fragmentName)
    {
        return MyPreferenceFragment.class.getName().equals(fragmentName);
    }


    public static class MyPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);
            //add the settings.xml layout
            addPreferencesFromResource(R.xml.prefs);

            // update the preferences if they change
            //setUpdatePreferences();
        }
    }
}
