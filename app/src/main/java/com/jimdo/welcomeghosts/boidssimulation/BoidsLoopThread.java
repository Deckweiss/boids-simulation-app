package com.jimdo.welcomeghosts.boidssimulation;

import android.content.SharedPreferences;
import android.graphics.Canvas;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BoidsLoopThread extends Thread {
    private double sepF, aliF, cohF;
    private int r, g, b, sizeBoids, senceRadius, speed;

    private double xTouch, yTouch;
    private int numberBoids = 0;
    private List<Boid> meineBoids = new ArrayList<>();
    private int sWidth, sHeight;
    static final long FPS = 60;
    private BoidsView view;
    private boolean running, isTouched, randomColours, attract;
    SharedPreferences prefs;


    public BoidsLoopThread(BoidsView view, SharedPreferences p, int height, int width) {
        running=isTouched=randomColours=attract=false;
        sHeight = height;
        sWidth = width;
        prefs = p;
        this.updatePrefs();
        this.view = view;
        view.setColours(r, g, b, randomColours);
    }

    public void setRunning(boolean run) {
        running = run;
    }

    public List<Boid> getMeineBoids() {
        return meineBoids;
    }

    public void setxTouch(double xTouch) {
        this.xTouch = xTouch;
    }

    public void setyTouch(double yTouch) {
        this.yTouch = yTouch;
    }

    public void setIsTouched(boolean isTouched) {
        this.isTouched = isTouched;
    }

    @Override
    public void run() {
        //android.os.Debug.waitForDebugger();  //remove this shit after debugging
        long ticksPS = 1000 / FPS;
        long startTime;
        long sleepTime;
        while (running) {
            Canvas c = null;
            startTime = System.currentTimeMillis();

            try {
                c = view.getHolder().lockCanvas();
                synchronized (view.getHolder()) {
                    view.onDraw(c);
                }
            } finally {
                if (c != null) {
                    view.getHolder().unlockCanvasAndPost(c);
                }
            }
            sleepTime = ticksPS - (System.currentTimeMillis() - startTime);

            try {
                if (sleepTime > 0)
                    sleep(sleepTime);
                else
                    sleep(10);
            } catch (Exception ignored) {
            }
        }
    }

    public void updatePrefs() {
        //Loading the preferences
        numberBoids = prefs.getInt("number_Boids", 0);
        sizeBoids = prefs.getInt("size_Boids", 0);
        senceRadius = prefs.getInt("sence_Radius", 0);
        cohF = prefs.getInt("cohesion", 0);
        aliF = prefs.getInt("alignment", 0);
        sepF = prefs.getInt("separation", 0);
        speed = prefs.getInt("speed_Boids", 0);
        randomColours = prefs.getBoolean("random_colours", false);
        attract = prefs.getBoolean("attracting_touch", false);
        if (!randomColours) {
            r = prefs.getInt("colour_r", 255);
            g = prefs.getInt("colour_g", 255);
            b = prefs.getInt("colour_b", 255);
        }
        meineBoids.clear();
        spawnBoids(numberBoids);
    }

    private void spawnBoids(int many) {
        //meineBoids.add(new Boid(100, 100, 200, 200, sepF, aliF, cohF, sizeBoids, speed));
        //meineBoids.add(new Boid(100+sizeBoids/2, 100+sizeBoids/2, 200, 200, sepF, aliF, cohF, sizeBoids, speed));

        Random r = new Random();
        for (int i = 1; i <= many; i++) {
            meineBoids.add(new Boid(r.nextInt(sWidth),
                                    r.nextInt(sHeight),
                                    r.nextInt(sWidth),
                                    r.nextInt(sHeight), sepF, aliF, cohF, sizeBoids, speed));
        }

    }


    public void sucheNachtbarn(Boid b) {
        b.getNeighbourBoids().clear();
        for (Boid n : meineBoids) {
            double abst = Math.sqrt(Math.pow(b.getxMid() - n.getxMid(), 2) + Math.pow(b.getyMid() - n.getyMid(), 2));
            if (abst <= senceRadius && abst > 0) {
                b.addNeighbour(n);
            }
        }
    }

    public void runBoid(Boid b, int width, int height) {
        b.run(width, height, isTouched, attract, xTouch, yTouch);
    }
}