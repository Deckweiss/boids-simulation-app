package com.jimdo.welcomeghosts.boidssimulation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class Boid {
    private float xPos, yPos, xNose, yNose, xDir, yDir, xDirt, yDirt, xMid, yMid, sepX, sepY, aliX, aliY, cohX, cohY, sLength;
    private int theSpeed;
    private int[] rgb;
    private double sepF, aliF, cohF;
    private List<Boid> neighbourBoids = new ArrayList<>();

    public Boid(int xPosition, int yPosition, int xDirection, int yDirection, double sep, double ali, double coh, int sLen, int theSpe){
        Random r=new Random();
        sLength = (float) (sLen+((r.nextInt(2)-1)*sLen*0.05));
        theSpeed= theSpe; //(int) (theSpe+((r.nextInt(2)-1)*theSpe*0.1));
        rgb = new int[]{r.nextInt(255), r.nextInt(255), r.nextInt(255)};
        xPos=xPosition;
        yPos=yPosition;
        sepF=sep/100;
        aliF=ali/100;
        cohF=coh/100;

        double lenght= Math.sqrt(Math.pow(xDirection-xPos,2)+ Math.pow(yDirection-yPos,2));
        sepX=sepY=aliX=aliY=cohX=cohY=0;
        xNose = xDir = (float) (((xDirection-xPos)/lenght) * sLength);
        yNose = yDir = (float) (((yDirection-yPos)/lenght) * sLength);
        xMid = (float) (xPos+0.5*xDir);
        yMid = (float) (yPos+0.5*yDir);
    }

    public void run(int width, int height, boolean touch, boolean attract, double xTouch, double yTouch){
        choseDir(touch, attract, xTouch, yTouch);
        step(width, height);
    }

    public void choseDir(boolean touch, boolean attr, double xTouch, double yTouch){
        float[] sepVec=separation();
        float[] aliVec=alignment();
        float[] cohVec=cohesion();
        float speedLimit;
        sepX = sepVec[0];
        sepY = sepVec[1];
        aliX = aliVec[0];
        aliY = aliVec[1];
        cohX = cohVec[0];
        cohY = cohVec[1];
        float touchAbstand = (float) Math.sqrt(Math.pow(xTouch-xMid,2)+Math.pow(yTouch-yMid,2));

        if(attr && touch && touchAbstand <= 11*sLength){
            xDirt= (float) (sepF * sepX + aliF * aliX *0.1 + cohF * cohX + -60/(xMid-xTouch));
            yDirt= (float) (sepF * sepY + aliF * aliY *0.1 + cohF * cohY + -60/(yMid-yTouch));
            speedLimit=theSpeed;
        }
        else if(!attr && touch && touchAbstand <= 11*sLength){
            float sepFac = (float) (Math.pow((touchAbstand-1),(1/4))*7-3);
            xDirt= (float) (sepF * sepX * sepFac + 350/(xMid-xTouch));
            yDirt= (float) (sepF * sepY * sepFac + 350/(yMid-yTouch));
            speedLimit= (float) theSpeed; // (theSpeed*(10*sLength)/touchAbstand*0.5);
        }
        else{
            xDirt= (float)((sepF * sepX + aliF * aliX + cohF * cohX) * 0.8);
            yDirt= (float)((sepF * sepY + aliF * aliY + cohF * cohY) * 0.8);
            speedLimit=theSpeed;
        }

        float lenghtt=(float)Math.sqrt(Math.pow(xDirt,2)+Math.pow(yDirt,2));
        if (lenghtt!=0) {
            xDir += xDirt / lenghtt * speedLimit / 10;
            yDir += yDirt / lenghtt * speedLimit / 10;
        }

        float lenght=(float)Math.sqrt(Math.pow(xDir,2)+Math.pow(yDir,2));
        if (lenght != 0) {
            xNose = xDir / lenght * sLength;
            yNose = yDir / lenght * sLength;
        }
        if(lenght > speedLimit){
            xDir= xDir/lenght*speedLimit;
            yDir= yDir/lenght*speedLimit;
        }
    }

    public float[] separation(){
        float x,y;
        x=y=0;
        int i = 0;
        for(Boid n:neighbourBoids){
            if(xMid - n.getxMid()==0 && yMid - n.getyMid()==0){
                //TODO chronenberg?
            }

            double minDist = 2 * sLength;
            double abst = Math.sqrt(Math.pow(xMid - n.getxMid(), 2) + Math.pow(yMid - n.getyMid(), 2));

            if(abst < minDist) {
                //abst /= minDist;
                //x += 3 * sLength / ((xMid - n.getxMid()) * (float) Math.pow(abst, 2));
                //y += 3 * sLength / ((yMid - n.getyMid()) * (float) Math.pow(abst, 2));
                x += 3 * sLength / (xMid - n.getxMid());
                y += 3 * sLength / (yMid - n.getyMid());
                i++;
            }
        }
        if(i != 0) {
            x /= i;
            y /= i;
        }
        return new float[]{x,y};
    }

    public float[] alignment(){
        int x=0;
        int y=0;
        int count=0;
        for(Boid n:neighbourBoids){
            x+=n.getxDir();
            y+=n.getyDir();
            count++;
        }
        if(count>0){
            x=x/count;
            y=y/count;
        }

        return new float[]{x,y};
    }

    public float[] cohesion(){
        float x=0;
        float y=0;
        int count=0;
        for(Boid n:neighbourBoids){
            x+=n.getxMid();
            y+=n.getyMid();
            count++;
        }
        if(count>0){
            x=xMid-(x/count);
            y=yMid-(y/count);
        }

        return new float[]{-x,-y};
    }

    public void step(int width, int height){
        xPos += xDir / 10;
        yPos += yDir / 10;
        xMid = (float) (xPos + (0.5 * xDir));
        yMid = (float) (yPos + (0.5 * yDir));
        if(xMid< -10){
            xPos=width;
        }
        else if(yMid< -10){
            yPos=height;
        }
        else if(xMid>width+10){
            xPos=0;
        }
        else if(yMid>height+10){
            yPos=0;
        }
    }
    public void addNeighbour(Boid n){
        neighbourBoids.add(n);
    }

    //<editor-fold desc="GetterMethods">
    public int[] getColour(){
        return rgb;
    }

    public float getxMid() {
        return xMid;
    }

    public float getyMid() {
        return yMid;
    }

    public float getyPos() {
        return yPos;
    }

    public float getxPos() {
        return xPos;
    }

    public float getyDir() {
        return yDir;
    }

    public float getxDir() {
        return xDir;
    }

    public float getxNose() {
        return xNose;
    }

    public float getyNose() {
        return yNose;
    }

    public List<Boid> getNeighbourBoids() {
        return neighbourBoids;
    }
/*
    public void setPanic(double xP, double yP) {
        this.panic = true;
        xPanic=xP;
        yPanic=yP;
    }
    public void setCalm() {
        this.panic = false;
    }
    */
//</editor-fold>
}
