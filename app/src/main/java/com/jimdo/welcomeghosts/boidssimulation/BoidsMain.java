package com.jimdo.welcomeghosts.boidssimulation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.Window;
import android.view.WindowManager;

import java.util.List;

public class BoidsMain extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

        //Fullscreen
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(new BoidsView(this, prefs, width, height));
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if ( keyCode == KeyEvent.KEYCODE_MENU ) {
            startActivity(new Intent(this, WallpaperPrefsActivity.class));
            // return 'true' to prevent further propagation of the key event
            return true;
        }
        String tag = this.findViewById(android.R.id.content).toString();
        if ( keyCode == KeyEvent.KEYCODE_BACK && tag.equals("prefs")){
            startActivity(new Intent(this, BoidsMain.class));
            // return 'true' to prevent further propagation of the key event
            return true;
        }
        // let the system handle all other key events
        return super.onKeyDown(keyCode, event);
    }
}

class BoidsView extends SurfaceView {
    private SurfaceHolder holder;
    private SharedPreferences prefs;
    private Paint mPaint = new Paint();
    private final Handler mHandler = new Handler();
    private BoidsLoopThread boidsLoopThread;
    private List<Boid> meineBoids;
    private int sHeight, sWidth;
    private boolean randomColours;
    private int r,g,b;

    public BoidsView(final Context context, SharedPreferences p, int width, int height) {
        super(context);
        prefs=p;
        sWidth=width;
        sHeight=height;
        boidsLoopThread = new BoidsLoopThread(this, prefs, sHeight, sWidth);
        holder = getHolder();
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                boolean retry = true;
                boidsLoopThread.setRunning(false);
                while (retry) {
                    try {
                        boidsLoopThread.join();
                        retry = false;
                    } catch (InterruptedException ignored) {
                    }
                }
            }



            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                //boidsLoopThread.updatePrefs();
                try {
                    boidsLoopThread.setRunning(true);
                    boidsLoopThread.start();
                } catch (IllegalThreadStateException e){
                    boidsLoopThread = null;
                    boidsLoopThread = new BoidsLoopThread(BoidsView.this, prefs, sHeight, sWidth);
                    boidsLoopThread.setRunning(true);
                    boidsLoopThread.start();
                }

            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }
        });
    }

    @Override
    protected void onDraw(Canvas c) {
        if (c != null) {
            //android.os.Debug.waitForDebugger();
            c.drawColor(Color.BLACK);
            c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
            meineBoids = boidsLoopThread.getMeineBoids();


            for (Boid b : meineBoids) {
                boidsLoopThread.sucheNachtbarn(b);
                boidsLoopThread.runBoid(b, c.getWidth(), c.getHeight());
                drawBoid(c, b);
            }
        }
    }

    public void drawBoid(Canvas c, Boid bo) {
        if(!randomColours){mPaint.setARGB(255,r,g,b);}
        else{
            int[] rgb=bo.getColour();
            mPaint.setARGB(255,rgb[0],rgb[1],rgb[2]);
        }
        
        //mPaint.setARGB(255,255,255,255);
        float xPos = bo.getxPos();
        float yPos = bo.getyPos();
        float xNose = bo.getxNose();
        float yNose = bo.getyNose();
        float xlp = xPos + (yNose / 3) - xNose / 9;
        float ylp = yPos + (-xNose / 3) - yNose / 9;
        float xrp = xPos + (-yNose / 3) - xNose / 9;
        float yrp = yPos + (xNose / 3) - yNose / 9;
        float[] pts = {xPos, yPos, xlp, ylp,
                xPos, yPos, xrp, yrp,
                xrp, yrp, xPos + xNose, yPos + yNose,
                xlp, ylp, xPos + xNose, yPos + yNose
                           /*, xPos, yPos,xPos+xDir, yPos+yDir*/};
        c.drawLines(pts, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent me) {
        try {
            Thread.sleep(25);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        switch (me.getAction()) {
            case MotionEvent.ACTION_DOWN:
                boidsLoopThread.setxTouch(me.getX());
                boidsLoopThread.setyTouch(me.getY());
                boidsLoopThread.setIsTouched(true);
                break;

            case MotionEvent.ACTION_MOVE:
                boidsLoopThread.setxTouch(me.getX());
                boidsLoopThread.setyTouch(me.getY());
                boidsLoopThread.setIsTouched(true);
                break;

            case MotionEvent.ACTION_UP:
                boidsLoopThread.setIsTouched(false);
                break;
            default:
                break;
        }
        return true;
    }

    public void setColours(int rv, int gv, int bv, boolean randomColoursv) {
        r=rv;
        g=gv;
        b=bv;
        randomColours=randomColoursv;
    }
}

/*
                try {
                    Toast.makeText(getContext(), String.valueOf(numberBoids), Toast.LENGTH_LONG).show();
                } catch (Exception e){
                    Toast.makeText(getContext(), "fggt, git shet dune: "+e, Toast.LENGTH_LONG).show();
                }
 */